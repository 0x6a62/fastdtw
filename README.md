# FastDtw

FastDtw algorithm implementation to compare distance between 2 series.

Reference: [FastDTW: Toward Accurate Dynamic Time Warping in Linear Time and Space](https://cs.fit.edu/~pkc/papers/tdm04.pdf)

# Usage

See the Sample project for a more detailed example.

```
dotnet add package FastDtw
```

```
open FastDtw

let series1 = [| 0.1; 2.3; 4.5; 6.7; 8.9 |] // float[]
let series2 = [| 0.2; 4.6; 8.0; 8.6; 6.4 |] // float[]
let radius = 2 
let distance1 = FastDtw.Distance series1 series2 2
let distance2 = Dtw.Distance series1 series2
```

# Development

```
# Build
dotnet build ./src

# Run tests
dotnet test ./tests

# Run tests (with more detail)
cd tests && dotnet run

# Package
dotnet pack ./src

# Sample
dotnet run ./sample
```

