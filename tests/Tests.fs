module Tests

open System
open Expecto
open FsCheck

open FastDtw
open FastDtw.Internal

let rand = Random()


/// Series generator
module Gen =
  /// Random series for comparison
  type SeriesArray = SeriesArray of float[] with
    member x.Value = match x with SeriesArray x -> x

  let seriesArrayArb() =
    // TODO: current only uses a fixed size
    let randLength () = rand.Next(100)
    Gen.arrayOfLength (randLength()) (Gen.sized <| (fun s -> Gen.choose (-s,s) |> Gen.map (fun x -> (float x) / 10.))) // Arb.generate
    |> Arb.fromGen
    |> Arb.convert SeriesArray (fun (SeriesArray x) -> x)

let config = { 
  FsCheckConfig.defaultConfig with 
    maxTest = 10000; 
    arbitrary = typeof<Gen.SeriesArray>.DeclaringType::FsCheckConfig.defaultConfig.arbitrary
  }

/// Test internal functions
[<Tests>]
let testsInternal =
  testList "FastDtw Library (Internal)" [

    testPropertyWithConfig config "min.int" <| fun (a: int) (b: int) ->
      min a b = min b a

    testPropertyWithConfig config "min.float" <| fun (a: NormalFloat) (b: NormalFloat) ->
      min a b = min b a
    
    testPropertyWithConfig config "min3.int.1" <| fun (a: int) (b: int) (c: int) ->
      min3 a b c = List.min [a; b; c]

    testPropertyWithConfig config "min3.int.2" <| fun (a: int) (b: int) (c: int) ->
      min3 a b c = min3 b a c && min3 a b c = min3 a c b && min3 a b c = min3 b c a

    testPropertyWithConfig config "min3.float.1" <| fun (a: NormalFloat) (b: NormalFloat) (c: NormalFloat) ->
      min3 a b c = List.min [a; b; c]

    testPropertyWithConfig config "min3.float.2" <| fun (a: NormalFloat) (b: NormalFloat) (c: NormalFloat) ->
      min3 a b c = min3 b a c && min3 a b c = min3 a c b && min3 a b c = min3 b c a

    testPropertyWithConfig config "max.int" <| fun (a: int) (b: int) ->
      max a b = max b a

    testPropertyWithConfig config "max.float" <| fun (a: NormalFloat) (b: NormalFloat) ->
      max a b = max b a

    testPropertyWithConfig config "coarser" <| fun (a: float[]) ->
      let v = coarser a
      v.Length = if a.Length % 2 = 0 then a.Length / 2 else a.Length / 2 + 1
    
    testCase "min2" <| fun _ ->
      let v = min 10 15
      Expect.equal v 10 "min"
    
    testCase "mean" <| fun _ ->
      let v = mean 10. 20.
      Expect.equal v 15. "mean"
  ]

/// Test public interface functions
[<Tests>]
let testsPublicInterface =
  testList "FastDtw Library (Public interface)" [
    testPropertyWithConfig config "fastdtw - sanity check" <| fun (series1: Gen.SeriesArray) (series2: Gen.SeriesArray) ->
      // printfn "%d %d" (series1.Value.Length) (series2.Value.Length)

      let radius = 2
      let series1 = series1.Value
      let series2 = series2.Value

      let (distance, path) = FastDtw.DistanceWithPath series1 series2 radius

      let correctDistance = distance >= 0.
      let startPosition = List.head path
      let correctStart = startPosition = (1,1)
      let endPosition = (List.rev >> List.head) path
      let correctEnd = endPosition = (series1.Length, series2.Length) 
      // Check if pathing skips indexing across the series
      let (_, _, pathNoJump) = 
        path 
        |> List.fold (fun (p1, p2, v) (x1,x2) ->
          (x1, x2, v && (abs (x1-p1) <= 1) && (abs (x2-p2) <= 1))) (1, 1, true)

      // Sanity checks on results
      correctDistance && correctStart && correctEnd && pathNoJump;

    testPropertyWithConfig config "fastdtw - sanity check2" <| fun (series1: Gen.SeriesArray) (series2: Gen.SeriesArray) ->
      let radius = 2
      let series1 = series1.Value
      let series2 = series2.Value

      let distanceFastDtw = FastDtw.FastDtw.Distance series1 series2 radius
      let distanceDtw = FastDtw.Dtw.Distance series1 series2 

      // FastDtw is an approximation that may be too high, this checks to make sure it's not less (that would be an error)
      distanceFastDtw >= distanceDtw
  ]


/// Performance comparison tests
[<Tests>]
let testsPerformance =
  /// Repeat function for performance test
  let inline repeat x f =
    for _ in [1..x] do 
      f()

  /// Make performance test function
  let makePerformanceTest length iterations radius =
    fun () ->
      let series1 = Array.init length (fun _ -> rand.NextDouble())
      let series2 = Array.init length (fun _ -> rand.NextDouble())
      Expect.isFasterThan 
        (fun () -> repeat iterations (fun () -> FastDtw.Distance series1 series2 radius |> ignore))
        (fun () -> repeat iterations (fun () -> Dtw.Distance series1 series2 |> ignore))
        "FastDtw is faster"

  /// Performance tests to run
  testList "FastDtw performance" [
    testCase "FastDtw is faster than Dtw (length=100)" <| (makePerformanceTest 100 5 2)
    testCase "FastDtw is faster than Dtw (length=500)" <| (makePerformanceTest 500 5 2)
    testCase "FastDtw is faster than Dtw (length=1000)" <| (makePerformanceTest 1000 5 2)
  ]
  
