﻿module Sample

open System
open FastDtw

/// Create a random series for dtw comparison
let randomSeries n =
  let rand = Random()
  [|1..n|] |> Array.map (fun x -> rand.NextDouble() * 5.)

[<EntryPoint>]
let main argv =
  let seriesLength = if argv.Length > 0 then Int32.Parse argv.[0] else 10

  let series1 = randomSeries seriesLength
  let series2 = randomSeries seriesLength

  let (distance, path) = FastDtw.DistanceWithPath series1 series2 2
  printfn "Distance (fastdtw): %f Path: %A" distance path

  let distance = FastDtw.Distance series1 series2 2
  printfn "Distance (fastdtw): %f" distance

  let distance = Dtw.Distance series1 series2
  printfn "Distance (dtw)    : %f" distance

  0
