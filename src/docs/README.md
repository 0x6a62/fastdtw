# FastDtw

FastDtw algorithm implementation to compare distance between 2 series.

Reference: [FastDTW: Toward Accurate Dynamic Time Warping in Linear Time and Space](https://cs.fit.edu/~pkc/papers/tdm04.pdf)

